# SchemeDemo

#### 介绍
唤端Demo，网页链接打开App，获取启动参数

AndroidPlugin AndroidStudio 工程（版本3.2.1）

UnityClient Unity 工程（版本2017.4.10f1）

Web 网页端

测试地址https://www.ganghood.net.cn/SchemeDemo.html 

目前只有安卓 2019.3.7

iOS 也OK 2019.3.8

对于iOS端：

1、设置Identifler和URL Schemes

2、iOS打包自动覆写UnityAppController.mm

当安卓onResume时获取参数 2019.3.14

效果图

![Image text](https://gitee.com/awnuxcvbn/SchemeDemo/raw/master/SchemeDemo.jpg)
