package cn.net.xuefei.schemedemo;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity {

    private String launchInfo="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Uri uri = getIntent().getData();
        if (uri != null) {
            // 完整的url信息
            String url = uri.toString();
            if (url!="")
            {
                launchInfo = url;
                Log.d("Unity","onCreate launchInfo:"+launchInfo);
                getIntent().setData(null);
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Uri uri = getIntent().getData();
        if (uri != null) {
            // 完整的url信息
            String url = uri.toString();
            if (url!="")
            {
                launchInfo = url;
                Log.d("Unity","onResume launchInfo:"+launchInfo);
                getIntent().setData(null);
            }
        }
    }

    public void getLaunchInfo()
    {
        UnityPlayer.UnitySendMessage("Main Camera", "OnLaunchInfo", launchInfo);
        launchInfo="";
    }
}
