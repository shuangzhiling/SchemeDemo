#import "UnityAppController.h"

@interface CustomAppController : UnityAppController
@end

IMPL_APP_CONTROLLER_SUBCLASS (CustomAppController)

@implementation CustomAppController

// 向Unity传递参数；
extern void UnitySendMessage(const char *, const char *, const char *);

NSString *URLString = @"";

extern "C"  
{  
    void _GetLaunchInfo();  
}  
  
void _GetLaunchInfo()  
{    
	//接有三方登录的要过滤回调，比如sharesdk 微信登录回调也会走openURL
    UnitySendMessage( "Main Camera", [@"OnLaunchInfo" UTF8String], [URLString UTF8String] );  
    // 清空，防止造成干扰;  
    URLString = @"";  
}  

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
    URLString = [url absoluteString];
    NSLog(@"openURL1 URLString:%@",URLString);
    return YES;
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{ 
    URLString = [url absoluteString];
    NSLog(@"openURL2 URLString:%@",URLString);
    return YES;
}
@end